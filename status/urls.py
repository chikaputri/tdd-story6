from django.conf.urls import url
from django.urls import re_path
from .views import index,add_status, index_2
from . import views

urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path('myprofile/', index_2, name='index_2'),
    re_path('add_status/', add_status, name = 'add_status'),
]