from django.test import TestCase, Client
from django.urls import resolve
from .views import index, add_status, index_2
from .models import Status
from django.http import HttpRequest
from django.utils import timezone

class Lab6UnitTest(TestCase):
    def test_lab_6_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    # def test_lab_6_url_exist_2(self):
        # response = Client().get('/myprofile/')
        # self.assertEqual(response.status_code,200)

    # def test_lab_6_url_not_exist(self):
        # response = Client().get('/')
        # self.assertNotEqual(response.status_code,404)

    # def test_lab6_using_index(self):
        # found = resolve('/')
        # self.assertEqual(found.func ,index)

    # def test_lab6_using_index_2(self):
        # found = resolve('/myprofile/')
        # self.assertEqual(found.func ,index_2)

    # def test_lab_6_using_lab_6_template(self):
        # response = Client().get('')
        # self.assertTemplateUsed(response, 'lab_6.html')

    # def test_lab_6_using_lab_6_template_2(self):
        # response = Client().get('/myprofile/')
        # self.assertTemplateUsed(response, 'my_profile.html')

    # def test_model_can_create_new_status(self):
        # #Creating a new Status
        # new_status = Status.objects.create(date=timezone.now(),status='Belajar ppw yuhu')

        # #Retrieving all available activity
        # counting_all_status = Status.objects.all().count()
        # self.assertEqual(counting_all_status,1)

    # def test_landing_page_is_completed(self):
    	# request = HttpRequest()
    	# response = index(request)
    	# html_response = response.content.decode('utf8')
    	# self.assertIn("Hello, Apa kabar?", html_response)
