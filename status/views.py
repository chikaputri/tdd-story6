from django.shortcuts import render, redirect
from .models import Status
from datetime import datetime
from .forms import Status_Form
from . import forms
from django.http import HttpResponseRedirect

response = {}
def index(request):
	response['form'] = forms.Status_Form
	response['status'] = Status.objects.all()
	html = 'lab_6.html'
	return render(request, html, response)

def index_2(request):
    return render(request, 'my_profile.html')

def add_status(request):
	form = forms.Status_Form(request.POST)
	if form.is_valid():
		response['status'] = request.POST['status']
		status = Status(status = response['status'])
		status.save()
		return HttpResponseRedirect('/')
	else:
		form = forms.Status_Form()

	return render(request, 'lab_6.html', response)

