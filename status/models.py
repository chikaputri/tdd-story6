from django.db import models
from datetime import datetime, date

# Create your models here.
class Status(models.Model):
	date = models.DateTimeField(default = datetime.now())
	status = models.TextField(max_length = 300)
