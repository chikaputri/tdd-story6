from django import forms
from .models import Status
from django.forms import ModelForm

class Status_Form(forms.ModelForm):
    status = forms.CharField(label='Status', required = True, widget = forms.TextInput())

    class Meta :
        model = Status
        fields = ('status',)
